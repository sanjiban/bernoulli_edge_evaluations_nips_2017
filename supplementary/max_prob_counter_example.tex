\section{Datasets with large disparity in region sizes}
\label{app:max_prob_reg}

In this section, we investigate scenarios where there is a large disparity in region sizes. We will show that in such scenarios, \algMaxProbReg has an arbitrarily poor performance. We will also show that unconstrained \algName vastly outperforms all other algorithms on such problems. 

We first examine the scenario as shown in Fig.~\ref{fig:max_prob_reg_counter_example}(a). There are two regions $\region_1$ and $\region_2$. $\region_1$ has only $1$ test $a$ with bias $\theta_a$. $\region_2$ has $T$ tests $\{ b_1, \dots, b_T \}$, each with bias $\theta_b$. The evaluation cost of each test is $1$. The following condition is enforced
\begin{equation}
	\theta_b^T = \theta_a + \varepsilon	
\end{equation} 

Under such conditions, the \algMaxProbReg algorithm would check tests in $\region_2$ before proceeding to $\region_1$. We compare the performance of this policy to the converse - one that evaluates $\region_1$ and then proceeds to $\region_2$.

Lets analyze the expected cost of \algMaxProbReg. If $\region_2$ is valid, it incurs a cost of $T$, else it incurs a cost of $T+1$. This equates to
\begin{equation}
\begin{aligned}
& \theta_b^T(T) + (1 - \theta_b^T)(T+1) \\
&= (T + 1) - \theta_b^T
\end{aligned}
\end{equation}

We now analyze the converse which selects test $a$. If $\region_1$ is valid, it incurs a cost of $1$, else it incurs $T+1$. This equates to 
\begin{equation}
\begin{aligned}
& \theta_a(1) + (1 - \theta_a)(T+1) \\
&= (T + 1) - \theta_aT
\end{aligned}
\end{equation}

\algMaxProbReg incurs a larger expected cost that equates to
\begin{equation}
\begin{aligned}
& (T + 1) - \theta_b^T - ( (T + 1) - \theta_aT ) \\
&= \theta_aT - \theta_b^T \\
&= \theta_aT - \theta_a - \varepsilon \\
& = \theta_a(T-1) - \varepsilon \\
\end{aligned}
\end{equation}
$T$ can be made arbitrarily large to push this quantity higher. 

We will now show that unconstrained \algName will evaluate $\region_1$ in this case. We apply the \algName selection rule in (\ref{eq:greedy_fdrd}) to this problem. The utility of selecting test $a$ is
\begin{equation}
\begin{aligned}
\gain{\drd}{a}{\obsOutcome} &=
(1 - \theta_a)(1 - \theta_b^T)  - \left[ \theta_a (1 - 1)(1 - \theta_b^T) \theta_a^2 + (1-\theta_a) (1 - 0) (1 - \theta_b^T) (1-\theta_a)^2  \right]
\\
&= (1 - \theta_a)(1 - \theta_b^T) - (1-\theta_a)^3(1 - \theta_b^T) \\
\end{aligned}
\end{equation}

The utility of selecting test $b_i$ is
\begin{equation}
\begin{aligned}
\gain{\drd}{b_i}{\obsOutcome} &=
(1 - \theta_a)(1 - \theta_b^T)  - \left[ \theta_b (1 - \theta_a)(1 - \theta_b^{T-1}) \theta_b^2 + (1-\theta_b) (1 - \theta_a) (1 - 0) (1-\theta_b)^2  \right]
\\
&= (1 - \theta_a)(1 - \theta_b^T) - \theta_b^3 (1 - \theta_a) (1 - \theta_b^{T-1}) -  (1-\theta_b)^3 (1 - \theta_a) \\
&= (1 - \theta_a)(1 - \theta_b^T) - (1 - \theta_a) [\theta_b^3  (1 - \theta_b^{T-1}) -  (1-\theta_b)^3 ] \\
\end{aligned}
\end{equation}

We assume that $T$ is sufficiently large such that $\theta_b \approx 1$. Then the difference is
\begin{equation}
\begin{aligned}
\gain{\drd}{a}{\obsOutcome} -  \gain{\drd}{b_i}{\obsOutcome}&=
(1 - \theta_a) [\theta_b^3  (1 - \theta_b^{T-1}) -  (1-\theta_b)^3 ] - (1-\theta_a)^3(1 - \theta_b^T) \\
&\approx (1 - \theta_a) [(1 - \theta_b^{T-1}) ] - (1-\theta_a)^3(1 - \theta_b^T) \\
&\approx (1 - \theta_a) (1 - \theta_b^T) [1 - (1-\theta_a)^2] \\
&\geq 0\\
\end{aligned}
\end{equation}

\begin{figure}[t]
    \centering
    \includegraphics[page=1,width=\textwidth]{max_prob_counter_example.pdf}
    \caption{%
    \label{fig:max_prob_reg_counter_example}
    (a) The scenario where unconstrained \algName outperforms \algMaxProbReg significantly. (b) The 2D motion planning scenario where unconstrained \algName outperforms others. Here the graph contains a straight line joining start and goal. With a low-probability, a block is placed between start and goal. This forces the path with many edges circumnavigating the block to have maximum probability. The straight line path joining start and goal has lower probability. Hence there is a similarity to the synthetic example in (a)} 
\end{figure}%

Hence unconstrained \algName would significantly outperform \algMaxProbReg in these problems. We now empirically show this result on a synthetic dataset as well as a carefully constructed 2D motion planning dataset. Table~\ref{tab:max_prob_res} shows a summary of these results. 

\begin{table}[!htpb]
\small
\centering
\caption{Normalized cost ($95\%$ C.I. lower / upper bound) with respect to unconstrained \algName}
\begin{tabulary}{\textwidth}{LCCCCC}\toprule
       & {\bf \algMVOI}       & {\bf \algRandom}       & {\bf \algMaxTally}       & {\bf \algSetCover}    & {\bf \algName} \\ 
       &                      & {Unconstrained}    & {Unconstrained}   & {Unconstrained}   & {Unconstrained}               \\
       &                      & {MaxProbReg}        & {MaxProbReg}      & {MaxProbReg}      & {MaxProbReg}                 \\ \midrule
Synthetic   &                    & $(6.50, 8.00)$  & $(5.50, 6.50)$  & $(3.00, 3.50)$  & $\red(0.00, 0.00)$    \\
($T:10$)    & $(3.00, 3.50)$     & $(3.00, 4.50)$  & $(5.00, 7.50)$  & $(3.00, 3.50)$  & $(3.00, 3.50)$  \\ 
2D Plan     &                    & $(9.50, 11.30)$ & $(2.80, 6.10)$  & $(6.60, 10.50)$  & $\red(0.00, 0.00)$   \\
($\numRegion:2$)  
            & $(6.60, 10.50)$    & $(6.90, 10.80)$ & $(6.80, 8.30)$  & $(6.60, 10.50)$  & $(7.30, 11.20)$  \\ 
2D Plan
            &                    & $(2.44, 3.17)$  & $(2.83, 3.28)$  & $(2.50, 2.56)$  & $\red(0.00, 0.00)$   \\
($\numRegion:19$)
            & $(0.89, 1.17)$	 & $(1.06, 1.28)$  & $(0.89, 0.94)$  & $(0.78, 0.94)$  & $(0.78, 0.89)$   \\ \bottomrule
\end{tabulary}
\label{tab:max_prob_res}
\end{table}

The first dataset is Synthetic which is a instantiation of the scenario shown in Fig.~\ref{fig:max_prob_reg_counter_example}(a). We set $\theta_a = 0.9$, $\theta_b = 0.9906$, $\varepsilon = 0.01$, $T = 10$. We see \algMaxProbReg does incurs $3$ times more cost than the unconstrained variant.

The second dataset is a motion planning dataset as shown in Fig.~\ref{fig:max_prob_reg_counter_example}(b). The dataset is created to closely resemble the synthetic dataset attributes. A RGG graph is created, and the straight line joining start and goal is added to the set of edges. A distribution of obstacles is created by placing a block with probability $0.3$. The first dataset has $2$ regions - the straight line containing one edge, and a path that goes around the block containing many more edges. Unconstrained \algName evaluates the straight line first. \algMaxProbReg evaluates the longer path first. We see \algMaxProbReg does incurs $7$ times more cost than the unconstrained variant.

The third dataset is same as the second, except the number of regions is increased to $19$. Now we see that the contrast reduces. \algMaxProbReg incurs $0.78$ fraction more cost than unconstrained version.