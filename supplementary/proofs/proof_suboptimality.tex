\section{Proof of Theorem \ref{thm:sub_opt}}
\label{sec:proof:sub_opt}

\begin{theorem*}
  Let $\pmin = \min_i P(\region_i)$, $\pminH = \min_{\hyp \in \hypSpace} P(\hyp)$ and $l = \max_i \abs{\region_i}$. The policy using (\ref{eq:cand_test_set:maxp}) has a suboptimality of $\alpha \left(2 \numRegion \log \left( \frac{1}{\pminH} \right) + 1 \right)$ where 
$\alpha \leq \left( 1 -   \max \left( (1 - \pmin)^2, \pmin^{\frac{2}{l}} \right) \right)^{-1}$.
\end{theorem*}

\begin{proof}
We start of by defining policies that do not greedily maximize $\fdrd{\obsOutcome}$

\begin{definition}
	Let an $\alpha$-approximate greedy policy be one that selects a test $\test'$ that satisfies the following criteria
	\begin{equation*}
	\gain{\drd}{\test'}{\obsOutcome} \geq \frac{1}{\alpha} \max\limits_{\test} \gain{\drd}{\test}{\obsOutcome}
	\end{equation*}
\end{definition}

We examine the scenarios where cost is uniform $\cost(\test) = 1$ for the sake of simplicity - the proof can be easily extended to non-uniform setting. We refer to the policy using (\ref{eq:cand_test_set:maxp}) as a test constraint as a \algMaxProbReg policy.

The marginal gain is evaluated as follows
\begin{equation}
\begin{aligned}
	\gain{\drd}{\test}{\obsOutcome} &= \mathbb{E}_{\outcomeTest{\test}} \left[ 
       \prod\limits_{r=1}^\numRegion  
      \left(1 - \prod\limits_{i \in (\region_r \cap \selTestSet)} \Ind(\outcomeVarTest{i} = 1) \prod\limits_{j \in (\region_r \setminus \selTestSet)} \biasTest{j} \right) \right.\\
      & - \left. \left( \prod\limits_{r=1}^\numRegion 
      \left(1 - \prod\limits_{i \in (\region_r \cap \selTestSet \cup \test)} \Ind(\outcomeVarTest{i} = 1) \prod\limits_{j \in (\region_r \setminus \selTestSet \cup \test)} \biasTest{j} \right) \right)
      ( \biasTest{t}^{\outcomeTest{\test}} (1-\biasTest{t})^{1-\outcomeTest{\test}} )^{2\sum\limits_{k=1}^{m} \Ind(\test \in \region_k)} \right]
\end{aligned}
\end{equation}

We will now bound $\alpha$, the ratio of marginal gain of the unconstrained greedy policy and \algMaxProbReg. 

\begin{equation}
\begin{aligned}
\alpha &\leq \frac{ \max\limits_{\test \in \candTestSet} \gain{\drd}{\test}{\obsOutcome} } { \min\limits_{\test \in \maxProbTestSet} \gain{\drd}{\test}{\obsOutcome} }
\end{aligned}
\end{equation}

The numerator and denominator contain $\prod\limits_{r=1}^\numRegion \left(1 - \prod\limits_{i \in (\region_r \cap \selTestSet)} \Ind(\outcomeVarTest{i} = 1) \prod\limits_{j \in (\region_r \setminus \selTestSet)} \biasTest{j} \right)$, the posterior probabilities of regions not being valid. Hence we normalize by dividing this term and expressing $\alpha$ in terms of a residual function $\rho (t | \obsOutcome)$

\begin{equation}
\begin{aligned}
\label{eq:subopt_bound}
\alpha &\leq \frac{ 1 - \min\limits_{\test \in \candTestSet} \rho(t | \obsOutcome)  } {1 - \max\limits_{\test \in \maxProbTestSet} \rho(t | \obsOutcome) }
\end{aligned}
\end{equation}

where the residual function $\rho (t | \obsOutcome)$ is
\begin{equation}
	\rho (t | \obsOutcome) = \frac{ \expect{\outcomeTest{\test}}{\left( \prod\limits_{r=1}^\numRegion 
      \left(1 - \prod\limits_{i \in (\region_r \cap \selTestSet \cup \test)} \Ind(\outcomeVarTest{i} = 1) \prod\limits_{j \in (\region_r \setminus \selTestSet \cup \test)} \biasTest{j} \right) \right)
      ( \biasTest{t}^{\outcomeTest{\test}} (1-\biasTest{t})^{1-\outcomeTest{\test}} )^{2\sum\limits_{k=1}^{m} \Ind(\test \in \region_k)}} }
      { \prod\limits_{r=1}^\numRegion \left(1 - \prod\limits_{i \in (\region_r \cap \selTestSet)} \Ind(\outcomeVarTest{i} = 1) \prod\limits_{j \in (\region_r \setminus \selTestSet)} \biasTest{j} \right) }
\end{equation}


\begin{figure}[t]
    \centering
    \includegraphics[page=1,width=\textwidth]{max_prob_reg_proof.pdf}
    \caption{%
    \label{fig:subopt_bound_scenario}
    The scenario where sub-optimality bound is maximized}
\end{figure}%

We will now claim that the bound is maximized in the scenario shown in Fig.~\ref{fig:subopt_bound_scenario}. The most likely region, $R_1$, is an isolated path which contains no tests in common with other regions. Let the probability of this region be $\pmin$ - this is the smallest probability that can be assigned to it. All other $\numRegion-1$ regions (of lower probabilty) share a common test $a$ of probability $\bias_a$. The remaining tests in these regions have probability $1$. Note that $\bias_a \leq \pmin$. In this scenario, the greedy policy will select the common test while the \algMaxProbReg will select a test from the most probably region. 

We will now show that this scenario allows us to realize the upper bound $1$ for the numerator in (\ref{eq:subopt_bound}). In other words, we will show that the residual function $\rho (a | \obsOutcome) \ll 1$ in our scenario.
\begin{equation}
\begin{aligned}
\rho (a | \obsOutcome) & \leq \frac{ \bias_a \pmin \prod\limits_{r=1}^{\numRegion-1} (1 - 1) \bias_a^{2(\numRegion-1)} \;+\; (1 - \bias_a) \pmin \prod\limits_{r=1}^{\numRegion-1} (1 - 0) (1 - \bias_a)^{2(\numRegion-1)} }
{ \pmin \prod\limits_{r=1}^{\numRegion-1} (1 - \bias_a) } \\
& \leq \frac{ (1 - \bias_a)^{2\numRegion - 1} } {  \prod\limits_{r=1}^{\numRegion-1} (1 - \bias_a) } \\
& \leq (1 - \bias_a)^\numRegion 
\end{aligned}
\end{equation}
We can drive $\rho (a | \obsOutcome) \ll 1$ by setting $\numRegion$ arbitrarily high.

We now show that scenario also allows us to bound the denominator in (\ref{eq:subopt_bound}) by maximizing  $\max\limits_{\test \in \region_1} \rho(t | \obsOutcome) $. We first note that by selecting a test that belongs only to one region, the residual is maximized. We have to figure out how large the residual can be. 
Let $\tau = \argmax\limits_{\test \in \region_1} \rho(t | \obsOutcome) $  be the most probable test with probability $\bias_\tau$. Let $\beta = \prod\limits_{\test \in \region_1, \test \neq \tau} \bias_t$ be the lumped probability of all other tests. Note that $\theta_\tau \beta = \pmin$. The residual can be expressed as
\begin{equation}
\begin{aligned}
\label{eq:demon_bound}
\rho (\tau | \obsOutcome) & \leq \frac{ \bias_\tau \prod\limits_{r=1}^{\numRegion-1} (1 - \bias_a) (1 - \beta) \bias_\tau^2 \;+\; (1 - \bias_\tau) \prod\limits_{r=1}^{\numRegion-1} (1 - \bias_a) (1 - \bias_\tau)^2 }
{ \prod\limits_{r=1}^{\numRegion-1} (1 - \bias_a) (1 - \bias_\tau \beta)} \\
& \leq \frac{ \bias_\tau^3 (1-\beta) + (1 - \bias_\tau)^3 } { (1 - \bias_\tau\beta) } \\
& \leq \frac{ \bias_\tau^3 (1-\beta) + \bias_\tau^2 - \bias_\tau^2 + (1 - \bias_\tau)^3 } { (1 - \bias_\tau\beta) } \\
& \leq \frac{ \bias_\tau^2 (1-\bias_\tau \beta) - \bias_\tau^2 (1 - \bias_\tau) + (1 - \bias_\tau)^3 } { (1 - \bias_\tau\beta) } \\
& \leq \bias_\tau^2 - \frac{ (1 - \bias_\tau)(2\bias_\tau - 1) } { (1 - \pmin) } \\
\end{aligned}
\end{equation}

This bound is concave and achieves maxima on the two extrema. In the first case, we assume $\beta = 0$, $\theta_\tau = \pmin$. This leads to 
\begin{equation}
\begin{aligned}
\rho (\tau | \obsOutcome) & \leq \pmin^2 - \frac{ (1 - \pmin)(2\pmin - 1) } { (1 - \pmin) } \\
& \leq \pmin^2 - (2\pmin - 1) \\
& (1 - \pmin)^2
\end{aligned}
\end{equation}

In the second case, we assume $\beta = \theta_\tau$. Lete $l$ be the maximum test in any region. Then $\theta_\tau = \pmin^{\frac{1}{l}}$. This leads to 
\begin{equation}
\begin{aligned}
\rho (\tau | \obsOutcome) & \leq \bias_\tau^2 - \frac{ (1 - \bias_\tau)(2\bias_\tau - 1) } { (1 - \pmin) } \\
& \leq \bias_\tau^2 \\
& \leq  \pmin^{\frac{2}{l}} \\
\end{aligned}
\end{equation}

Combining these we have
\begin{equation}
	\label{eq:rho_bound}
	\rho (\tau | \obsOutcome) \leq \max \left( (1 - \pmin)^2, \pmin^{\frac{2}{l}} \right)
\end{equation}

Substituting (\ref{eq:rho_bound}) in (\ref{eq:subopt_bound}) we have
\begin{equation}
	\label{eq:final_alpha}
	\alpha \leq \frac{1}{ 1 -   \max \left( (1 - \pmin)^2, \pmin^{\frac{2}{l}} \right)} 
\end{equation}

We now use Theorem 11 in \citet{golovin2011adaptive} to state that an $\alpha$-approximate greedy policy $\policy$ optimizing $\fdrd{\obsOutcome}$ enjoys the following guarantee
\begin{equation}
	\cost(\policy) \leq \alpha \cost(\policyOpt) \left(2 \numRegion \log\left(\frac{1}{\pminH}\right) + 1\right) 
\end{equation}

\end{proof}