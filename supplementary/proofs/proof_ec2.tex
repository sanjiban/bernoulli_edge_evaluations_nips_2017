\section{Proof of Lemma \ref{lem:ec2}}
\label{sec:proof:lem_ec2}
\begin{lemma*} 
The expression $\fec{\obsOutcome}$ is strongly adaptive monotone and adaptive submodular.
\end{lemma*}

\begin{proof}
The proof for $\fec{\obsOutcome}$ is a straight forward application of Lemma 5 from \citet{golovin2010near}. We now adapt the proof of adaptive submodularity from Lemma 6 in \citet{golovin2010near}

We first prove the result for uniform prior. To prove adaptive submodularity, we must show that for all $\outcomeTestSet{\mathcal{A}} < \outcomeTestSet{\mathcal{B}}$ and $\test \in \testSet$ , we have $\gain{\ectext}{\test}{\outcomeTestSet{\mathcal{A}}} \geq \gain{\ectext}{\test}{\outcomeTestSet{\mathcal{B}}}$. Fix $\test$ and $\obsOutcome$, and let $\mathcal{V}(\obsOutcome) = \setst{\hyp}{P(\hyp | \obsOutcome) > 0} $ denote the version space, if $\obsOutcome$ encodes the observed outcomes. Let $\nv = \abs{\mathcal{V}(\obsOutcome)}$ be the number of hypotheses in the version space. 
Likewise, let $n_{i,a}(\obsOutcome) = \abs{ \set{\hyp : \hyp \in \mathcal{V}(\obsOutcome, \outcomeVarTest{\test} = a) \cap \hypSpaceR_i} }$, 
and let $n_a(\obsOutcome) = \sum\limits_{i=1}^{l} n_{i,a}(\obsOutcome)$. 
We define a function $\phi$ of the quantities ${n_{i,a} : 1 \leq i \leq l,a \in \{0,1\}}$ such that $\gain{\ectext}{\test}{\obsOutcome} = \phi(n(\obsOutcome))$, where $n(\obsOutcome)$ is the vector consisting of $n_{i,a}(\obsOutcome)$ for all $i$ and $a$. For brevity, we suppress the dependence of $\obsOutcome$ where it is unambiguous.

It will be convenient to define $e_a$ to be the number of edges cut by $\test$ such that at $\test$ both hypotheses agree with each other but disagree with the realized hypothesis $\hyp*$, conditioning on $\outcomeVarTest{\test} = a$. Written as a function of $n$, we have  $e_a = \sum\limits_{i < j}\sum\limits_{b \neq a} n_{i,b} n_{j,b}$.

We also define $\gamma_a$ to be the number of edges cut by $\test$ corresponding to self-edges belonging to hypotheses that disagree with the realized hypothesis $\hyp^*$, conditioning on $\outcomeVarTest{\test} = a$. Written as a function of $n$, we have  $\gamma_a = \sum\limits_{i} \sum\limits_{b \neq a} n_{i,b}^2$. 

\begin{equation}
	\phi(\nvec) = \sum\limits_{i < j} \sum\limits_{a \neq b} n_{i,a} n_{j,b} + \sum\limits_a e_a \left( \frac{n_a}{\nv} \right) + \sum\limits_a \gamma_a \left( \frac{n_a}{\nv} \right)
\end{equation}

where $e_a = \sum\limits_{i < j}\sum\limits_{b \neq a} n_{i,b} n_{j,b}$ and $\gamma_a = \sum\limits_{i} \sum\limits_{b \neq a} n_{i,b}^2$.
Here, $i$ and $j$ range over all class indices, and $a$ and $b$ range over all possible outcomes of test $t$. The first term on the right-hand side counts the number of edges that will be cut by selecting test $t$ no matter what the outcome of $t$ is. Such edges consist of hypotheses that disagree with each other at $t$ and, as with all edges, lie in different classes. The second term counts the expected number of edges cut by $t$ consisting of hypotheses that agree with each other at $t$. Such edges will be cut by $t$ iff they disagree with $h^*$ at $t$. The third term counts the expected number of edges cut by $t$ consisting of hypothesis with self-edges that disagree with $h^*$ at $t$.


We need to show $\frac{\partial \phi}{\partial n_{k,c}} \geq 0$ according to proof of Lemma 6 in \citet{golovin2010near}.

\begin{equation}
	\label{eq:ec2:deriv}
	\frac{\partial \phi}{\partial n_{k,c}} = 
	\frac{\partial}{\partial n_{k,c}} \left( \sum\limits_{i < j} \sum\limits_{a \neq b} n_{i,a} n_{j,b}  \right) +
	\frac{\partial}{\partial n_{k,c}} \left( \sum\limits_a e_a \left( \frac{n_a}{\nv} \right) \right) + 
	\frac{\partial}{\partial n_{k,c}} \left( \sum\limits_a \gamma_a \left( \frac{n_a}{\nv} \right) \right)
\end{equation}

Expanding the first term in (\ref{eq:ec2:deriv})
\begin{equation}
	\frac{\partial}{\partial n_{k,c}} \left( \sum\limits_{i < j} \sum\limits_{a \neq b} n_{i,a} n_{j,b}  \right)
	= \sum\limits_{i \neq k, a \neq c} n_{i,a}
\end{equation}

Expanding the second term in (\ref{eq:ec2:deriv})
\begin{equation}
	\frac{\partial}{\partial n_{k,c}} \left( \sum\limits_a e_a \left( \frac{n_a}{\nv} \right) \right)
	= \sum\limits_{i \neq k, a \neq c} \frac{ n_a n_{i,c} }{\nv} - \sum\limits_{b} \frac{e_b n_b}{\nv^2} + \frac{e_c}{\nv}
\end{equation}

Expanding the third term in (\ref{eq:ec2:deriv})
\begin{equation}
\begin{aligned}
	\frac{\partial}{\partial n_{k,c}} \left( \sum\limits_a \gamma_a \left( \frac{n_a}{\nv} \right) \right)
	&= \frac{\partial}{\partial n_{k,c}} \left( \frac{n_c}{\nv} \gamma_c \right) + \sum\limits_{a \neq c} \frac{\partial}{\partial n_{k,c}} \left( \frac{n_a}{\nv} \gamma_a \right) \\
	&= \frac{n_c}{\nv} \underbrace{\frac{\partial}{\partial n_{k,c}} \gamma_c}_{= 0} + \frac{\gamma_c}{\nv}  \underbrace{\frac{\partial}{\partial n_{k,c}} n_c}_{= 1} - \gamma_c n_c \frac{\partial}{\partial n_{k,c}} \left( \frac{1}{\nv} \right) + \sum\limits_{a \neq c} \frac{\partial}{\partial n_{k,c}} \left( \frac{n_a}{\nv} \gamma_a \right)\\
	&=  \frac{\gamma_c}{\nv} - \frac{ \gamma_c n_c }{\nv^2} +  \sum\limits_{a \neq c} \frac{\partial}{\partial n_{k,c}} \left( \frac{n_a}{\nv} \gamma_a \right)\\
	&=  \frac{\gamma_c}{\nv} - \frac{ \gamma_c n_c }{\nv^2} +  
	\sum\limits_{a \neq c} \left(
	\frac{n_a}{\nv} \underbrace{\left( \frac{\partial}{\partial n_{k,c}} \gamma_a \right)}_{= 2n_{k,c}} + 
	\frac{\gamma_a}{\nv} \underbrace{ \frac{\partial}{\partial n_{k,c}} n_a }_{=0} + 
	\gamma_a n_a \frac{\partial}{\partial n_{k,c}} \frac{1}{\nv}
	\right) \\
	&=  \frac{\gamma_c}{\nv} - \frac{ \gamma_c n_c }{\nv^2} +  
	\sum\limits_{a \neq c} \left( 2\frac{n_a n_{k,c}}{\nv} - \frac{\gamma_a n_a}{\nv^2}  \right) \\
	&= \frac{\gamma_c}{\nv} + 2 n_{k,c} \sum\limits_{a \neq c} \frac{n_a}{\nv} - \sum\limits_b \frac{\gamma_b n_b}{\nv^2} \\
\end{aligned}
\end{equation}

Putting it all together

\begin{equation}
	\label{eq:ec2:deriv2}
	\frac{\partial \phi}{\partial n_{k,c}} = 
	\frac{(e_c + \gamma_c)}{\nv} + 
	\sum\limits_{i \neq k, a \neq c} \frac{ n_a n_{i,c} }{\nv} +
	2 n_{k,c} \sum\limits_{a \neq c} \frac{n_a}{\nv} +
	\sum\limits_{i \neq k, a \neq c} n_{i,a}
	- \sum\limits_{b} \frac{e_b n_b}{\nv^2} - \sum\limits_b \frac{\gamma_b n_b}{\nv^2}
\end{equation}

Multiplying (\ref{eq:ec2:deriv2}) by $\nv$ we see it is non negative iff

\begin{equation}
	\sum\limits_b \frac{(e_b + \gamma_b) n_b}{\nv} \leq e_c + \gamma_c + \sum\limits_{a \neq c, i \neq k} n_a n_{i,c} + 2 n_{k,c} \sum\limits_{a \neq c} n_a + \nv \sum\limits_{a \neq c, i \neq k} n_{i,a} 
\end{equation}

Expanding LHS we get

\begin{equation}
\begin{aligned}
\sum\limits_b \frac{(e_b + \gamma_b) n_b}{\nv} &=  \frac{(e_c + \gamma_c) n_c}{\nv} + \sum\limits_{b \neq c} \frac{(e_b + \gamma_b) n_b}{\nv} \\
&\leq e_c + \frac{\gamma_c n_c}{\nv} + \sum\limits_{b \neq c} \frac{(e_b + \gamma_b) n_b}{\nv} \\
&\leq e_c + \frac{\gamma_c n_c}{\nv} + \sum\limits_{b \neq c} \frac{n_b}{\nv} \left( \sum\limits_{i<j}\sum\limits_{a \neq b} n_{i,a} . n_{j,a} + \sum\limits_i \sum\limits_{a\neq b} n_{i,a}^2 \right) \\
&\leq e_c + \frac{\gamma_c n_c}{\nv} + \sum\limits_{b \neq c} \frac{n_b}{\nv} \left( \sum\limits_{i<j} n_{i,c} . n_{j,c} + \sum\limits_i n_{i,c}^2 \right) + \sum\limits_{b \neq c} \frac{n_b}{\nv} \left( \sum\limits_{i<j}\sum\limits_{a \neq b,c} n_{i,a} . n_{j,a} + \sum\limits_i \sum\limits_{a\neq b,c} n_{i,a}^2 \right)\\
&\leq e_c + 
\underbrace{\sum\limits_{b \neq c} \frac{n_b}{\nv} \left( \sum\limits_{i<j} n_{i,c} . n_{j,c} + \sum\limits_i n_{i,c}^2 \right)}_{\text{\textcircled{A}}} + \underbrace{\sum\limits_{b \neq c} \frac{n_b}{\nv} \left( \sum\limits_{i<j}\sum\limits_{a \neq b,c} n_{i,a} . n_{j,a} + \sum\limits_i \sum\limits_{a\neq b,c} n_{i,a}^2 \right) + \frac{\gamma_c n_c}{\nv}}_{\text{\textcircled{B}}} \\
\end{aligned}
\end{equation}

If $\{x_i\}_{i \geq 0}$ be a finite sequence of non-negative real numbers. Then for any $k$
\begin{equation} 
\label{eq:sumprod}
	\sum\limits_{i < j} x_i x_j + \sum\limits_i x_i^2 \leq \left( \sum_i x_i \right) \left( \sum_{i \neq k} x_i \right) + x_k^2
\end{equation}

Using (\ref{eq:sumprod}) and expanding \textcircled{A} we have
\begin{equation}
\label{eq:terma}
\begin{aligned}
&\sum\limits_{b \neq c} \frac{n_b}{\nv} \left( \sum\limits_{i<j} n_{i,c} . n_{j,c} + \sum\limits_i n_{i,c}^2 \right) \\
\leq&\sum\limits_{b \neq c} \frac{n_b}{\nv} \left( \left( \sum_i n_{i,c} \right) \left( \sum_{i \neq k} n_{i,c} \right) + n_{k,c}^2 \right) \\
\leq&\sum\limits_{b \neq c} \frac{n_b}{\nv} \left( n_c \left( \sum_{i \neq k} n_{i,c} \right) + n_{k,c}^2 \right) \\
\leq& \sum\limits_{b \neq c} \frac{n_b n_c}{\nv} \left( \sum_{i \neq k} n_{i,c} \right) + \sum\limits_{b \neq c} \frac{n_b}{\nv} n_{k,c}^2  \\
\leq& \sum\limits_{b \neq c} n_b \left( \sum_{i \neq k} n_{i,c} \right) + \sum\limits_{b \neq c} \frac{n_b n_c}{\nv} n_{k,c}  \\
\leq& \sum\limits_{a \neq c, i \neq k} n_a n_{i,c} + \sum\limits_{b \neq c} n_b n_{k,c}  \\
\leq& \sum\limits_{a \neq c, i \neq k} n_a n_{i,c} + 2 n_{k,c} \sum\limits_{a \neq c} n_a   \\
\end{aligned}
\end{equation}

Using (\ref{eq:sumprod}) and expanding \textcircled{B} we have
\begin{equation}
\label{eq:termb}
\begin{aligned}
& \sum\limits_{b \neq c} \frac{n_b}{\nv} \left( \sum\limits_{i<j}\sum\limits_{a \neq b,c} n_{i,a} . n_{j,a} + \sum\limits_i \sum\limits_{a\neq b,c} n_{i,a}^2 \right) + \frac{\gamma_c n_c}{\nv} \\
\leq& \sum\limits_{b \neq c} \frac{n_b}{\nv} \left( 
\sum\limits_{a \neq b,c} \left( \sum_{i \neq k} n_{i,a} \right) n_a
+ \sum\limits_{a\neq b,c} n_{k,a}^2 \right) + \frac{\gamma_c n_c}{\nv} \\
\leq&\sum\limits_{b \neq c} \frac{n_b}{\nv} \nv \sum\limits_{a \neq c} \sum_{i \neq k} n_{i,a} +
\sum\limits_{b \neq c} \frac{n_b}{\nv} \sum\limits_{a\neq b,c} n_{k,a}^2  + \frac{\gamma_c n_c}{\nv} \\
\leq&\sum\limits_{b \neq c} n_b \sum\limits_{a \neq c, i \neq k} n_{i,a} +
\sum\limits_{b \neq c} \frac{n_b}{\nv} \sum\limits_{a\neq c} n_{k,a}^2  + \frac{\gamma_c n_c}{\nv} \\
\leq&\nv \sum\limits_{a \neq c, i \neq k} n_{i,a} +
\sum\limits_{b \neq c} \frac{n_b}{\nv} \gamma_c  + \frac{\gamma_c n_c}{\nv} \\
\leq&\nv \sum\limits_{a \neq c, i \neq k} n_{i,a} +
\gamma_c \left( \sum\limits_{b \neq c} \frac{n_b}{\nv}   + \frac{n_c}{\nv} \right)\\
\leq&\nv \sum\limits_{a \neq c, i \neq k} n_{i,a} +
\gamma_c \\
\end{aligned}
\end{equation}

Combining (\ref{eq:terma}) and (\ref{eq:termb})

\begin{equation}
\begin{aligned}
\sum\limits_b \frac{(e_b + \gamma_b) n_b}{\nv} &\leq e_c + \sum\limits_{a \neq c, i \neq k} n_a n_{i,c} + 2 n_{k,c} \sum\limits_{a \neq c} n_a + 
\nv \sum\limits_{a \neq c, i \neq k} n_{i,a} +
\gamma_c
\end{aligned}
\end{equation}

Hence the inequality $\frac{\partial \phi}{\partial n_{k,c}} \geq 0$ holds. For non-uniform prior, the proofs from Lemma 6 in \citet{golovin2010near} carry over. 

\end{proof}