\section{Proof of Theorem \ref{thm:set_cover}}
\label{sec:proof:set_cover}

\begin{theorem*}
\algSetCover is a near-optimal policy for checking all regions. 
\end{theorem*}

We present a refined version of the theorem that we will prove.

\begin{theorem*}
Let $\policy$ be the \algSetCover policy, which is a partial mapping from observation vector $\obsOutcome$ to tests, such that it terminates only when all regions $\region_i$ are either completely evaluated or invalidated. Let the expected cost of such a policy be $\cost(\policy)$.
Let $\policyOpt$ be the optimal policy for checking all regions. Let $\numTest = \abs{\testSet}$ be the number of tests.
\algSetCover enjoys the following guarantee
\begin{equation*}
	\cost(\policy) \leq \cost(\policyOpt) ( \log (\numTest) + 1) 
\end{equation*}
\end{theorem*}

\begin{proof}
We will prove this by drawing an equivalence of the problem to a special case of \emph{stochastic set coverage} with non-uniform costs, showing \algSetCover greedily solves this problem, and using a guarantee for a greedy policy as presented in \citet{golovin2011adaptive}. 

The stochastic set coverage problem is as follows - there is a ground set of elements $U$, and items $E$ such that item $e$ is associated with a distribution over subsets of $U$. When an item is selected, a set is sampled from its distribution. The problem is to adaptively select items until all elements of $U$ are covered by sampled sets, while minimizing the expected number of items selected. Here we consider the case where a cost is associated with each item.

We now show that the problem of selecting tests to invalidate other tests is equivalent to stochastic set coverage. The ground set is the set of all tests $\testSet$. The item set has a one to one correspondence with the test set $\testSet$. 
Let $\hat{f}(\obsOutcome)$ be the utility function measuring coverage of tests given selected tests and outcomes $\obsOutcome$. This is defined as 
\begin{equation}
	\hat{f}(\obsOutcome) = \abs{ \selTestSet \cup \set{\bigcup\limits_{i=1}^\numRegion \setst{\region_i}{P(\region_i | \obsOutcome) = 0}}} 
\end{equation}
The expected gain in utility when selecting a test $\test$ is as follows - with probability $\bias_\test$ if a $\test$ outcome is true, only $\test$ is covered. With probability $1 - \bias_\test$, if a test outcome is false, tests that belong to regions being invalidated are covered. This can be expressed formally as follows. Given $\test \notin \set{\bigcup\limits_{i=1}^\numRegion \setst{\region_i}{P(\region_i | \obsOutcome) = 0}}$, the expected gain $\gain{\hat{f}}{\test}{\obsOutcome}$ is
\begin{equation}
\begin{aligned}
&\gain{\hat{f}}{\test}{\obsOutcome} = \expect{\outcome_\test}{ \hat{f}(\obsOutcomeAdd{\test}) - \hat{f}(\obsOutcome) } \\
&= \expect{\outcome_\test}{ \abs{ \selTestSet \cup \set{\test} \cup \set{\bigcup\limits_{i=1}^\numRegion \setst{\region_i}{P(\region_i | \obsOutcomeAdd{\test}) = 0}}} -
\abs{ \selTestSet \cup \set{\bigcup\limits_{i=1}^\numRegion \setst{\region_i}{P(\region_i | \obsOutcome) = 0}}} } \\
&= P(\outcomeVarTest{\test} = 1) \times 1 + \\
& P(\outcomeVarTest{\test} = 0) \times \left(1 + 
\abs{  \set{\bigcup\limits_{i=1}^\numRegion \setst{\region_i}{P(\region_i | \obsOutcome) > 0} - 
  \bigcup\limits_{j=1}^\numRegion \setst{\region_j}{P(\region_j | \obsOutcome, \outcomeVarTest{\test} = 0) > 0}   }
  \setminus \set{\selTestSet \cup \set{\test}} } \right)\\
&= 1 + (1 - \bias_{\test})  
\abs{  \set{\bigcup\limits_{i=1}^\numRegion \setst{\region_i}{P(\region_i | \obsOutcome) > 0} - 
  \bigcup\limits_{j=1}^\numRegion \setst{\region_j}{P(\region_j | \obsOutcome, \outcomeVarTest{\test} = 0) > 0}   }
  \setminus \set{\selTestSet \cup \set{\test}} } \\
\end{aligned}
\end{equation}
\algSetCover is an adaptive greedy policy with respect to $\gain{\hat{f}}{\test}{\obsOutcome}$ as shown
\begin{equation}
\begin{aligned}
\optTest &\in \argmaxprob{\test \in \testSet}\; \gain{\hat{f}}{\test}{\obsOutcome} \\
&\in \argmaxprob{\test \in \testSet}\; (1 - \bias_{\test})  
\abs{  \set{\bigcup\limits_{i=1}^\numRegion \setst{\region_i}{P(\region_i | \obsOutcome) > 0} - 
  \bigcup\limits_{j=1}^\numRegion \setst{\region_j}{P(\region_j | \obsOutcome, \outcomeVarTest{\test}=0) > 0}   }
  \setminus \set{\selTestSet \cup \set{\test}} } \\
\end{aligned}
\end{equation}
Note that $\hat{f}(\groundtruth) = \numTest$ is the maximum value the utility can attain. Let $\policyOpt$ be the optimal policy.
Since $\hat{f}(\obsOutcome)$ is a strong adaptive monotone submodular function, we use Theorem 15 in \citet{golovin2011adaptive} to state the following guarantee
\begin{equation*}
	\cost(\policy) \leq \cost(\policyOpt) ( \log (\numTest) + 1) 
\end{equation*}
\end{proof}