\section{Introduction}
\label{sec:intro}
This paper addresses a class of robotic motion planning problems where \emph{path evaluation is expensive}. 
Classic approaches~\citep{Lav06,canny1988complexity,bellman2013dynamic} do not explicitly minimize evaluation effort. 
%This problem fundamentally recurs in many domains. 
For example, in robot arm planning~\citep{dellin2016guided}, evaluation requires expensive geometric intersection computations. 
In UAV path planning~\citep{cover2013sparse}, evaluation must be done online with limited computational resources (Fig.~\ref{fig:marquee}).

State of the art planning algorithms~\citep{dellin2016unifying} first compute a set of unevaluated paths quickly, and then evaluate them sequentially to find a valid path. %Typically, the candidate paths are produced by searching over a graph of feasible transitions.
Oftentimes, candidate paths share common edges. Hence, evaluation of a small number of edges can provide information about the validity of many candidate paths simultaneously. Methods that check paths sequentially, however, do not reason about these common edges.

This leads us naturally to the \emph{feasible path identification} problem - given a library of candidate paths, identify a valid path while minimizing the cost of edge evaluations. We assume access to a prior distribution over edge validity, which encodes how obstacles are distributed in the environment (Fig.~\ref{fig:marquee}(a)). As we evaluate edges and observe outcomes, the uncertainty of a candidate path collapses. 

Our first key insight is that this problem is equivalent to \emph{decision region determination (DRD)}~\citep{javdani2014near, chen2015submodular}) - given a set of tests (edges), hypotheses (validity of edges), and regions (paths), the objective is to drive uncertainty into a single decision region. This linking enables us to leverage existing methods in Bayesian active learning for robotic motion planning.

\citet{chen2015submodular} provide a method to solve this problem by maximizing an objective function that satisfies \emph{adaptive submodularity}~\citep{golovin2011adaptive} - a natural diminishing returns property that endows greedy policies with near-optimality guarantees. Unfortunately, naively applying this algorithm requires $\bigo{2^E}$ computation to select an edge to evaluate, where $E$ is the number of edges in all paths.

We define the \probBernDRD problem, which leverages additional structure in robotic motion planning by assuming edges are independent Bernoulli random variables~\footnote{Generally, edges in this graph are correlated, as edges in collision are likely to have neighbours in collision. Unfortunately, even measuring this correlation is challenging, especially in the high-dimensional non-linear configuration space of robot arms. Assuming independent edges is a common simplification~\citep{Lav06, narayanan2017heuristic, choudhury2016pareto, burns2005sampling,dellin2016unifying}}, and regions correspond to sets of edges evaluating to true. We propose \emph{\algFullName (\algName)}, which provides a greedy policy to select candidate edges in $\bigo{E}$. We prove our surrogate objective also satisfies adaptive submodularity~\citep{golovin2011adaptive}, and provides the same bounds as \citet{chen2015submodular} while being more efficient to compute. 

\begin{figure}[!t]
    \centering
    \includegraphics[page=1,width=0.8\textwidth]{marquee.pdf}
    \caption{%
    \label{fig:marquee}
    The feasible path identification problem (a) The explicit graph contains dynamically feasible maneuvers~\citep{pivtoraiko2009differentially} for a UAV flying fast, with a set candidate paths. The map shows the distribution of edge validity for the graph. (b) Given a distribution over edges, our algorithm checks an edge, marks it as invalid (red) or valid (green), and updates its belief. We continue until a feasible path is identified as free. We aim to minimize the number of expensive edge evaluations. 
    \fullFigGap}
\end{figure}%

We make the following contributions:
\begin{enumerate}
  \item We show a novel equivalence between feasible path identification and the DRD problem, linking motion planning to Bayesian active learning.
  \item We develop \algName, a near-optimal algorithm for the special case of Bernoulli tests, which selects tests in $\bigo{E}$ instead of $\bigo{2^E}$.
  \item We demonstrate the efficacy of our algorithm on a spectrum of planning problems for mobile robots, manipulators, and real flight data collected from a full scale helicopter. 
\end{enumerate}
