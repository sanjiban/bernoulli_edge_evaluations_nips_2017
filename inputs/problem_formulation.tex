\section{Problem Formulation}
\label{sec:problem_formulation}

\subsection{Planning as Feasible Path Identification on Explicit Graphs}
Let $\explicitGraph = \pair{\vertexSet}{\edgeSet}$ be an explicit graph that consists of a set of vertices $\vertexSet$ and edges $\edgeSet$. Given a pair of start and goal vertices, $\pair{\start}{\goal} \in \vertexSet$, a search algorithm computes a path $\Path \subseteq \edgeSet$ - a connected sequence of valid edges. To ascertain the validity of an edge, it invokes an evaluation function $\evalFn: \edgeSet \rightarrow \{0, 1\}$. We address applications where edge evaluation is expensive, i.e., the computational cost $\cost(\edge)$ of computing $\evalFn(\edge)$ is significantly higher than regular search operations\footnote{It is assumed that $\cost(\edge)$ is modular and non-zero. It can scale with edge length.}.

We define a world as an outcome vector $\world \in \{0, 1 \}^{\abs{\edgeSet}}$ which assigns to each edge a boolean validity when evaluated, i.e. $\evalFn(\edge) = \world(\edge)$.
We assume that the outcome vector is sampled from an independent Bernoulli distribution $P(\world)$, giving rise to a \emph{Generalized Binomial Graph (GBG) }~\citep{frieze2015introduction}.
% Choosing this distribution class simplifies analysis and leads to an efficient posterior update. While this assumption is violated in motion planning due to local spatial correlations, Section \ref{sec:experiments} shows this assumption works in practice.

We make a second simplification to the problem - from that of search to that of identification. Instead of searching $\explicitGraph$ online for a path, we frame the problem as identifying a valid path from a library of `good' candidate paths $\PathSet = \seq{\Path}{\numRegion}$. The candidate set of paths $\PathSet$ is constructed offline, while being cognizant of $P(\world)$, and can be verified to ensure that all paths have acceptable solution quality when valid. \footnote{Refer to supplementary on various methods to construct a library of good candidate paths} Hence we care about completeness with respect to $\PathSet$ instead of $\explicitGraph$. 

We wish to design an adaptive edge selector $\selectFn(\world)$ which is a decision tree that operates on a world $\world$, selects an edge for evaluation and branches on its outcome. The total cost of edge evaluation is $\cost(\selectFn(\world))$. Our objective is to minimize the cost required to find a valid path:
\begin{equation}
\label{eq:e4gbg}
\min \; \expect{ \world \in P(\world) }{ \cost(\selectFn(\world)) }  \; \mathrm{s.t} \; \forall \world , \exists \Path \; : \; \prod\limits_{\edge \in \Path} \world(\edge) = 1 \; , \; \Path \subseteq \selectFn(\world)
\end{equation}

\subsection{Decision Region Determination with Independent Bernoulli Tests}
%The probability mass of all the version space can evaluated as $P(\hypSpace(\obsOutcome)) = \prod\limits_{i \in \selTestSet} \biasTest{i}^{\obsOutcome(i)} (1 - \biasTest{i})^{1 - \obsOutcome(i)}$. 

%We can now view a region $\region$ as a version space  $\regionH \subseteq \hypSpace$ consistent with its constituent tests. Hence, let the \emph{relevant version space} be $\hypSpaceR(\obsOutcome) = \setst{\hyp \in \hypSpace}{ \forall \test \in \selTestSet \cap \region, \hyp(\test) = \obsOutcome(\test)}$. The set of all hypotheses in $\regionH$ consistent with relevant outcomes in $\obsOutcome$ is given by $\regionH \cap \hypSpaceR(\obsOutcome)$ \sjnote{is this different from  $\hypSpaceR(\obsOutcome)$?}. We can define a similar set of expressions for a version space $\Not{\regionH}$ where a region is \emph{not} valid. The terms $P(\regionH \cap \hypSpaceR(\obsOutcome))$ and $P(\Not{\regionH} \cap \hypSpaceR(\obsOutcome))$ allows us to quantify the progress made on determining region validity. 
We now define an equivalent problem - \emph{\probBernDRDFull (\probBernDRD)}. Define a set of tests $\testSet = \{1,\dots,\numTest\}$, where the outcome of each test is a Bernoulli random variable $\outcomeVarTest{\test} \in \outcomeSpace$, $P(\outcomeVarTest{\test} = \outcomeTest{\test}) = \biasTest{\test}^{\outcomeTest{\test}} (1 - \biasTest{\test})^{1 - \outcomeTest{\test}}$. We define a set of hypotheses $\hyp \in \hypSpace$, where each is an outcome vector $\hyp \in \outcomeSpace^\testSet$ mapping all tests $\test \in \testSet$ to outcomes $\hyp(\test)$. We define a set of regions $\set{\region_i}_{i=1}^{\numRegion}$, each of which is a subset of tests $\region \subseteq \testSet$. A region is determined to be valid if all tests in that region evaluate to true, which has probability $P(\region) = \prod\limits_{\test \in \region} P(\outcomeVarTest{\test} = 1)$. \vspace{-0.5em}

If a set of tests $\selTestSet \subseteq \testSet$ are performed, let the observed outcome vector be denoted by $\obsOutcome \in \outcomeSpace^{\abs{\selTestSet}}$. Let the \emph{version space} $\hypSpace(\obsOutcome)$ be the set of hypotheses consistent with observation vector $\obsOutcome$, i.e. $\hypSpace(\obsOutcome) = \setst{\hyp \in \hypSpace}{ \forall \test \in \selTestSet, \hyp(\test) = \obsOutcome(\test)}$.


We define a policy $\policy$ as a mapping from observation vector $\obsOutcome$ to tests. A policy terminates when it shows that at least one region is valid, or all regions are invalid. Let $\groundtruth \in \outcomeSpace^\testSet$ be the ground truth - the outcome vector for all tests. Denote the observation vector of a policy $\policy$ given ground truth $\groundtruth$ as $\obsOutcomeFunc{\policy}{\groundtruth}$. The expected cost of a policy $\policy$ is $\cost(\pi) = \expect{\groundtruth}{\cost(\obsOutcomeFunc{\policy}{\groundtruth}}$ where $c(\obsOutcome)$ is the cost of all tests $\test \in \selTestSet$. The objective is to compute a policy $\policyOpt$ with minimum cost that ensures at least one region is valid, i.e.
\begin{equation}
\label{eq:drd}
\policyOpt \in \argminprob{\policy} \;\cost(\policy) \; \mathrm{s.t} \; \forall \groundtruth , \exists \region_d \; : \; P(\region_d \;|\; \obsOutcomeFunc{\policy}{\groundtruth}) = 1
\end{equation}
Note that we can cast problem (\ref{eq:e4gbg}) to (\ref{eq:drd}) by setting $\edgeSet=\testSet$ and $\PathSet=\set{\region_i}_{i=1}^{\numRegion}$. That is, driving uncertainty into a region is equivalent to identification of a valid path (Fig.~\ref{fig:bern_drd_problem}). This casting enables us to leverage efficient algorithms with near-optimality guarantees for motion planning. %We will henceforth be addressing \probBernDRD.
