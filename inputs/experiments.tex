\section{Experiments} \vspace{-0.7em}
\label{sec:experiments}
We evaluate \algName on a collection of datasets spanning across a spectrum of synthetic problems and real-world planning applications. 
The synthetic problems are created by randomly selecting problem parameters to test the general applicability of \algName.
The motion planning datasets range from simplistic yet insightful 2D problems to more realistic high dimension problems as encountered by an UAV or a robot arm. 
The 7D arm planning dataset is obtained from a high fidelity simulation as shown in Fig.~\ref{fig:real_world_planning}(a).
Finally, we test \algName on experimental data collected from a full scale helicopter flying that has to avoid unmapped wires at high speed as it comes into land as shown in Fig.~\ref{fig:real_world_planning}(b).
Refer to supplementary for exhaustive details on experiments and additional results. \footnote{We plan to provide a link to open source code and datasets for the camera ready version.}
%\sjnote{Seems like some datasets are missing? I would also sell this a little harder, especially for the real problems}

\subsection{Heuristic approaches to solving the \probBernDRD problem} \vspace{-0.7em}
\label{sec:heuristics}
%\sjnote{Move this to experiments section, make it a subsection?}
We propose a collection of competitive heuristics that can also be used to solve the \probBernDRD problem. These heuristics are various $\texttt{SelectTest}(\candTestSet, \biasVec, \obsOutcome)$ policies in the framework of Alg.~\ref{alg:drd_skeleton}. To simplify the setting, we assume unit cost $\cost(\test) = 1$ although it would be possible to extend these to nonuniform setting. The first heuristic \algRandom selects a test by sampling uniform randomly
\begin{equation}
  \label{eq:policy_random}
  \optTest \in \candTestSet
\end{equation}
We adopt our next heuristic \algMaxTally from \citet{dellin2016unifying} where the test belonging to most regions is selected. It uses the following criteria, which exhibits a `fail-fast' characteristic
\begin{equation}
  \label{eq:policy_max_tally}
  \optTest   \in  \argmaxprob{\test \in \candTestSet}\; \sum\limits_{i=1}^{\numRegion} \Ind\left(\test \in \region_i, P(\region_i | \obsOutcome) > 0 \right)
\end{equation}
The next policy \algSetCover selects tests that maximize the expected number of `covered' tests, i.e. if a selected test is in collision, how many other tests does it remove from consideration.
\begin{equation}
  \label{eq:policy_set_cover}
  \optTest \in  \argmaxprob{\test \in \candTestSet}\; 
  (1 - \biasTest{\test})
  \abs{ 
  \set{\bigcup\limits_{i=1}^\numRegion \setst{\region_i}{P(\region_i | \obsOutcome) > 0} - 
  \bigcup\limits_{j=1}^\numRegion \setst{\region_j}{P(\region_j | , \substack{\obsOutcome, \\ \outcomeVarTest{\test} = 0} ) > 0}   }
  \setminus \set{\selTestSet \cup \set{\test}}
  } 
\end{equation}
\begin{theorem}
\algSetCover is a near-optimal policy for the problem of optimally checking all regions. 
\end{theorem}
The last baseline is derived from a classic heuristic in decision theory: myopic value of information (\citet{howard1966information}). 
\algMVOI greedily chooses the test that maximizes the change in the probability mass of the most likely region. This test selection works only with $\texttt{SelectCandTestSet}(\obsOutcome) = \maxProbTestSet$. 
\begin{equation}
  \label{eq:policy_mvoi}
  \optTest   \in  \argmaxprob{\test \in \maxProbTestSet}\; (1 - \biasTest{\test}) \max\limits_{i = 1, \dots, \numRegion} P(\region_i \;|\; \obsOutcome, \outcomeVarTest{\test} = 0)
\end{equation}



\begin{figure}[t]
    \centering
    \includegraphics[page=1,width=\textwidth]{2d_env_comparison.pdf}
    \caption{%
    \label{fig:2d_env_comparison}
    Performance (number of evaluated edges) of all algorithms on 2D geometric planning. Snapshots, at interim and final stages respectively, show evaluated valid edges (green), invalid edges (red) and the final path (magenta). The utility of edges as computed by algorithms is shown varying from low (black) to high (cream).}
\end{figure}%

\begin{figure}[t]
    \centering
    \includegraphics[page=1,width=\textwidth]{real_world_planning.pdf}
    \caption{%
    \label{fig:real_world_planning}
    (a) A 7D arm has to perform pick and place tasks at high speed in a table with clutter. (b) Experimental data from a full-scale helicopter that has to react quickly to avoid unmapped wires detected by the sensor. \algName (given an informative prior) checks a small number of edges around the detected wire and identifies a path. (c) Scenario where regions have size disparity. Unconstrained \algName significantly outperforms other algorithms on such a scenario.
    \fullFigGap}
\end{figure}%

\input{figs/table2}
\vspace{-0.7em}
\subsection{Analysis of results} \vspace{-0.7em}
\label{sec:experiments:discussion}
Table~\ref{tab:benchmark_results} shows the evaluation cost of all algorithms on various datasets normalized w.r.t \algName. The two numbers are lower and upper $95\%$ confidence intervals - hence it conveys how much fractionally poorer are algorithms w.r.t \algName. The best performance on each dataset is highlighted. We present a set of observations to interpret these results.
\begin{observation}
\algName has a consistently competitive performance across all datasets.
\end{observation} \vspace{-0.7em}

Table~\ref{tab:benchmark_results} shows that on $13$ out of the $14$ datasets, \algName is at par with the best. On $7$ of those it is exclusively the best. 

\begin{observation}
The \algMaxProbReg variant improves the performance of all algorithms on most datasets
\end{observation} \vspace{-0.7em}

Table~\ref{tab:benchmark_results} shows that this is true on $12$ datasets. The impact is greatest on \algRandom on the 2D Forest dataset - performance improves from $(19.45, 27.66)$ to $(0.13, 0.30)$. However, this is not true in general. On datasets with large disparity in region sizes as illustrated in Fig.~\ref{fig:real_world_planning}(c), unconstrained \algName significantly outperforms other algorithms. In such scenarios, \algMaxProbReg latches on to the most probable path which also happens to have a large number of edges. It performs poorly on instances where this region is invalid, while the other region containing a single edge is valid. Unconstrained \algName prefers to evaluate the single edge belonging to region 1 before proceeding to evaluate region 2, performing optimally on those instances. Hence, the myopic nature of \algMaxProbReg is the reason behind its poor performance.
\begin{observation}
On planning problems, \algName strikes a trade-off between the complimentary natures of \algMaxTally and \algMVOI.
\end{observation} \vspace{-0.7em}

We examine this in the context of 2D planning as shown in Fig.~\ref{fig:2d_env_comparison}. 
\algMaxTally selects edges belonging to many paths which is useful for path elimination but does not reason about the event when the edge is not in collision.
\algMVOI selects edges to eliminate the most probable path but does not reason about how many paths a single edge can eliminate. 
\algName switches between these behaviors thus achieving greater efficiency than both heuristics.

\begin{observation}
\algName checks informative edges in collision avoidance problems encountered a helicopter
\end{observation} \vspace{-0.7em}

Fig.~\ref{fig:real_world_planning}(b) shows the efficacy of \algName on experimental flight data from a helicopter avoiding wire.