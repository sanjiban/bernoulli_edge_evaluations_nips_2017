\section{The Bernoulli Subregion Edge Cutting Algorithm} \label{sec:algorithm}
The DRD problem in general is addressed by the \emph{Decision Region Edge Cutting (\direct)}~\citep{chen2015submodular} algorithm. The intuition behind the method is as follows - as tests are performed, hypotheses inconsistent with test outcomes are pruned away. Hence, tests should be incentivized to push the probability mass over hypotheses into \emph{any region} as fast as possible. \citet{chen2015submodular} derive a surrogate objective function that provides such an incentive by \emph{creating separate sub-problems} for each region and combining them in a Noisy-OR fashion such that \emph{quickly solving any one sub-problem suffices}. Importantly, this objective is adaptive submodular~\citep{golovin2011adaptive} - greedily maximizing such an objective results in a near-optimal policy. 

We adapt the framework of \direct to address the \probBernDRD problem. We first provide a modification to the \ecsq sub-problem objective which is simpler to compute when the distribution over hypotheses is non-uniform, while providing the same guarantees. Unfortunately, naively applying \direct requires $\bigo{2^\testSet}$ computation per sub-problem. For the special case of independent Bernoulli tests, we present a more efficient \emph{Bernoulli Subregion Edge Cutting (\algName)} algorithm, which computes each subproblem in $\bigo{\testSet}$ time. %Finally, we show how this computation can further be reduced to $\bigo{1}$ between iterations. 
We provide a brief exposition deferring to the supplementary for detailed derivations.

\subsection{A simple subproblem: One region versus all}
Following \citet{chen2015submodular}, we define a `one region versus all' subproblem, the solution of which helps address the \probBernDRD. Given a \emph{single region}, the objective is to either push the version space to that region, or collapse it to a single hypothesis. We view a region $\region$ as a version space $\regionH \subseteq \hypSpace$ consistent with its constituent tests. We define this subproblem over a set of \emph{disjoint subregions} $\subregion_i$. Let the hypotheses in the target region $\regionH$ be $\subregion_1$. Every other hypothesis $\hyp \in \Not{\regionH}$ is defined as its own subregion $\subregion_i, i>1$,  where $\Not{\regionH}$ is a set of hypothesis where a region is \emph{not} valid. Determining which subregion is valid falls under the framework of \emph{Equivalence Class Determination} (ECD), (a special case of the DRD problem) and can be solved efficiently by the \ecsq algorithm (\citet{golovin2010near}). This objective defines a graph with nodes as subregions and edges between distinct subregions, where the weight of an edge is the product of probabilities of subregions. As tests are performed and outcomes are received, the version space shrinks, and probabilities of different subregions are driven to 0. This has the effect of decreasing the total weight of edges. Importantly, the problem is solved i.f.f. the weight of all edges is zero. The weight over the set of subregions is:
% The weight of all edges is:
% \sjnote{it's a little weird that later we use $\fec{argument}$ differently then $\ec$ here. One possibility: define a set of edges, define this as weights over them. You could even define this as a weight over the set of subregions, since that is what you define over.}
\begin{equation}
\label{eq:weight_golovin}
\wecgolovin(\{\subregion_i\}) %= \sum\limits_{i \neq j}  \weight(\subregion_i \times \subregion_j) 
= \sum\limits_{j \neq k} P(\subregion_j) P(\subregion_k)
\end{equation}
When hypotheses have uniform weight, this can be computed efficiently for the `one region versus all' subproblem. Let $P(\Not{\subregion_1}) =  \sum\limits_{i>1} P(\subregion_i)$:
\begin{equation}
    \wecgolovin(\{\subregion_i\}) = P(\subregion_1) P(\Not{\subregion_1}) + P(\Not{\subregion_1})\left(P(\Not{\subregion_1}) - \frac{1}{|\hypSpace|}\right)
\end{equation}
For non-uniform prior however, this quantity is more difficult to compute. We modify this objective slightly, adding self-edges on subregions $\subregion_{i}, i>1$, enabling more efficient computation while still maintaining the same guarantees:
\begin{equation}
\label{eq:weight_sub}
\begin{aligned}
    \wec(\{\subregion_i\}) &= P(\subregion_1) (\sum\limits_{i\neq1} P(\subregion_i)) + (\sum\limits_{i\neq1} P(\subregion_i)) (\sum\limits_{j \geq 1} P(\subregion_j)) \\
    &= P(\subregion_1) P(\Not{\subregion_1}) + P(\Not{\subregion_1})^2 = P(\Not{\regionH}) (P(\regionH) + P(\Not{\regionH}))
\end{aligned}
\end{equation}
% \sjnote{Seems unecessary to define subregions? You compute all the quantities you need with $P(R)$ and $P(\Not R)$. Makes this more confusing}
For region $\region$, let the \emph{relevant version space} be $\hypSpaceR(\obsOutcome) = \setst{\hyp \in \hypSpace}{ \forall \test \in \selTestSet \cap \region, \hyp(\test) = \obsOutcome(\test)}$. The set of all hypotheses in $\regionH$ consistent with relevant outcomes in $\obsOutcome$ is given by $\regionH \cap \hypSpaceR(\obsOutcome)$. The terms $P(\regionH \cap \hypSpaceR(\obsOutcome))$ and $P(\Not{\regionH} \cap \hypSpaceR(\obsOutcome))$ allows us to quantify the progress made on determining region validity. Naively computing these terms would require computing all hypotheses and assigning them to correct subregions, thus requiring a runtime of $\bigo{2^\testSet}$. However, for the special case of Bernoulli tests, we can reduce this to $\bigo{\testSet}$ as we can see from the expression 
\begin{equation}
\wec(\{\subregion_i\} \cap \hypSpaceR(\obsOutcome) ) = \left(1 - \prod\limits_{i \in (\region \cap \selTestSet)} \Ind(\outcomeVarTest{i} = 1)
      \prod\limits_{j \in (\region \setminus \selTestSet)} \biasTest{j} \right)
      \left( \prod\limits_{k \in \region \cap \selTestSet} \biasTest{k}^{\obsOutcome(k)} (1 - \biasTest{k})^{1 - \obsOutcome(k)} \right)^2
\end{equation}
We can further reduce this to $\bigo{1}$ when iteratively updated (see supplementary for derivations).
We now define a criterion that incentivizes removing edges quickly and has theoretical guarantees. Let $\fec{\obsOutcome}$ be the weight of edges removed on observing outcome vector $\obsOutcome$. This is evaluated as
\begin{equation}
\begin{aligned}
\label{eq:fec_applied}
\fec{\obsOutcome} &= 1 - \frac{ \wec(\{\subregion_i\} \cap \hypSpaceR(\obsOutcome) ) }{ \wec(\{\subregion_i\}) }  \\
&= 1 - \frac{\left(1 - \prod\limits_{i \in (\region \cap \selTestSet)} \Ind(\outcomeVarTest{i} = 1)
      \prod\limits_{j \in (\region \setminus \selTestSet)} \biasTest{j} \right)
      \left( \prod\limits_{k \in \region \cap \selTestSet} \biasTest{k}^{\obsOutcome(k)} (1 - \biasTest{k})^{1 - \obsOutcome(k)} \right)^2
}{1 - \prod\limits_{i \in \region} \biasTest{i}}
\end{aligned}
\end{equation}

\begin{lemma}
The expression $\fec{\obsOutcome}$ is strongly adaptive monotone and adaptive submodular.
\end{lemma}

\subsection{Solving the \probBernDRD problem using \algName }

We now return to \probBernDRD problem (\ref{eq:drd}) where we have multiple regions $\{ \region_1, \dots, \region_\numRegion \}$ that overlap. Each region $\region_r$ is associated with an objective $\feci{r}{\obsOutcome}$ for solving the `one region versus all' problem. Since solving any one such subproblem suffices, we combine them in a \emph{Noisy-OR} formulation by defining an objective $\fdrd{\obsOutcome} = 1 - \prod\limits_{r=1}^\numRegion (1 - \feci{r}{\obsOutcome})$~\citep{chen2015submodular} which evaluates to
\begin{equation}
\label{eq:fdrd_applied}
 1 - \prod\limits_{r=1}^\numRegion \left(  \frac{\left(1 - \prod\limits_{i \in (\region_r \cap \selTestSet)} \Ind(\outcomeVarTest{i} = 1)
      \prod\limits_{j \in (\region_r \setminus \selTestSet)} \biasTest{j} \right)
      \left( \prod\limits_{k \in \region_r \cap \selTestSet} \biasTest{k}^{\obsOutcome(k)} (1 - \biasTest{k})^{1 - \obsOutcome(k)} \right)^2
}{1 - \prod\limits_{i \in \region_r} \biasTest{i}}  \right)
\end{equation}
Since $\fdrd{\obsOutcome} = 1$ iff $\feci{r}{\obsOutcome} = 1$ for at least one $r$, we define the following surrogate problem to \probBernDRD 
\begin{equation}
\label{eq:direct_drd}
\policyOpt \in \argminprob{\policy} \;\cost(\policy) \; \mathrm{s.t} \; \forall \groundtruth \; : \;  \fdrd{\obsOutcomeFunc{\policy}{\groundtruth}} \geq 1
\end{equation}
The surrogate problem has a structure that allows greedy policies to have near-optimality guarantees
\begin{lemma}
The expression $\fdrd{\obsOutcome}$ is strongly adaptive monotone and adaptive submodular.
\end{lemma}

\begin{theorem}
\label{eq:drd_near_opt}
Let $\numRegion$ be the number of regions, $\pminH$ the minimum prior probability of any hypothesis, $\policy_{DRD}$ be the greedy policy and $\policyOpt$ with the optimal policy. Then $\cost(\policy_{DRD}) \leq \cost(\policy^*)(2\numRegion \log \frac{1}{\pminH} + 1)$.
\end{theorem}

\IncMargin{1em}
\begin{algorithm}[t]
  \caption{ Decision Region Determination with Independent Bernoulli Test$\left( \set{\region_i}_{i=1}^{\numRegion}, \biasVec, \groundtruth \right)$ }\label{alg:drd_skeleton}
  \algorithmStyle
  $\selTestSet \gets \emptyset$ \;
  \While{$(\nexists \region_i, P(\region_i | \obsOutcome) = 1) $ \textbf{\upshape and} $(\exists \region_i, P(\region_i | \obsOutcome) > 0) $}
  {
    $\candTestSet \gets \texttt{SelectCandTestSet}(\obsOutcome)$ \Comment*[r]{Using either (\ref{eq:cand_test_set:all}) or (\ref{eq:cand_test_set:maxp})}
    $\optTest \gets \texttt{SelectTest}(\candTestSet, \biasVec, \obsOutcome)$ \Comment*[r]{Using either (\ref{eq:greedy_fdrd}),(\ref{eq:policy_random}),(\ref{eq:policy_max_tally}),(\ref{eq:policy_set_cover}) or (\ref{eq:policy_mvoi})}  
    $\selTestSet \gets \selTestSet \cup \optTest$\; 
    $\outcomeTest{\optTest} \gets \groundtruth(\optTest)$ \Comment*[r]{Observe outcome for selected test}
  }
\end{algorithm}

We now describe the \algName algorithm. Algorithm \ref{alg:drd_skeleton} shows the framework for a general decision region determination algorithm. In order to specify \algName, we need to define two options - a candidate test set selection function $\texttt{SelectCandTestSet}(\obsOutcome)$ and a test selection function $\texttt{SelectTest}(\candTestSet, \biasVec, \obsOutcome)$. The unconstrained version of \algName implements $\texttt{SelectCandTestSet}(\obsOutcome)$ to return the set of all tests $\candTestSet$ that contains only unevaluated tests belonging to active regions
\begin{equation}
  \label{eq:cand_test_set:all}
  \candTestSet = \set{\bigcup\limits_{i=1}^\numRegion \setst{\region_i}{P(\region_i | \obsOutcome) > 0}}
  \setminus \selTestSet
\end{equation}
We now examine the \algName test selection rule $\texttt{SelectTest}(\candTestSet, \biasVec, \obsOutcome)$   
\begin{equation}
\begin{aligned}
\label{eq:greedy_fdrd}
\test^*  &\in  \argmaxprob{\test \in \candTestSet}\; \frac{1}{\cost(\test)} \mathbb{E}_{\outcomeTest{\test}} \left[ 
       \prod\limits_{r=1}^\numRegion  
      \left(1 - \prod\limits_{i \in (\region_r \cap \selTestSet)} \Ind(\outcomeVarTest{i} = 1) \prod\limits_{j \in (\region_r \setminus \selTestSet)} \biasTest{j} \right) \right.\\
      & - \left. \left( \prod\limits_{r=1}^\numRegion 
      \left(1 - \prod\limits_{i \in (\region_r \cap \selTestSet \cup \test)} \Ind(\outcomeVarTest{i} = 1) \prod\limits_{j \in (\region_r \setminus \selTestSet \cup \test)} \biasTest{j} \right) \right)
      ( \biasTest{t}^{\outcomeTest{\test}} (1-\biasTest{t})^{1-\outcomeTest{\test}} )^{2\sum\limits_{k=1}^{m} \Ind(\test \in \region_k)} \right]
\end{aligned}
\end{equation}
The intuition behind this update is that tests are selected to squash the probability of regions not being valid. It also additionally incentivizes selection of tests on which multiple regions overlap. 
\subsection{Adaptively constraining test selection to most likely region}
We observe in our experiments that the surrogate (\ref{eq:fdrd_applied}) suffers from a slow convergence problem - $\fdrd{\obsOutcome}$ takes a long time to converge to $1$ when greedily optimized. 
%This can be attributed to the curvature of the function (Fig.~\ref{fig:histogram_max_prob}).  
To alleviate the convergence problem, we introduce an alternate candidate selection function $\texttt{SelectCandTestSet}(\obsOutcome)$ that assigns to $\candTestSet$ the set of all tests that belong to the most likely region $\maxProbTestSet$ which is evaluated as follows (we will refer to this variant as \algMaxProbReg)
\begin{equation}
  \label{eq:cand_test_set:maxp}
  \maxProbTestSet = \set{\argmaxprob{\region_i = \seq{\region}{\numRegion}}  \;P(\region_i | \obsOutcome) }
  \setminus \selTestSet
\end{equation}
Applying the constraint in (\ref{eq:cand_test_set:maxp}) leads to a dramatic improvement for any test selection policy as we will show in Sec.~\ref{sec:experiments:discussion}. The following theorem offers a partial explanation
\begin{theorem}
A policy that greedily latches to a region according the the posterior conditioned on the region outcomes has a near-optimality guarantee of 4 w.r.t the optimal region evaluation sequence.
\end{theorem}
Applying the constraint in (\ref{eq:cand_test_set:maxp}) implies we are no longer greedily optimizing $\fdrd{\obsOutcome}$. However, the following theorem bounds the sub-optimality of this policy. 
\begin{theorem}
  Let $\pmin = \min_i P(\region_i)$, $\pminH = \min_{\hyp \in \hypSpace} P(\hyp)$ and $l = \max_i \abs{\region_i}$. The policy using (\ref{eq:cand_test_set:maxp}) has a suboptimality of $\alpha \left(2 \numRegion \log \left( \frac{1}{\pminH} \right) + 1 \right)$ where 
$\alpha \leq \left( 1 -   \max \left( (1 - \pmin)^2, \pmin^{\frac{2}{l}} \right) \right)^{-1}$.
\end{theorem} \vspace{-0.5em}
